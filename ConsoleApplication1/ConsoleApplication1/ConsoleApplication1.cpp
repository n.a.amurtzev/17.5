﻿

#include "iostream"

class Vector
{

public:
	Vector() : x(0), y(0), z(0)
	{}
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}
	void Show()
	{
		std::cout << " X == " << x << " Y == " << y << " Z == " << z << "\n";
	}
	void ShowM()
	{
		std::cout << sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2)) << '\n';
	}

private:
	double x;
	double y;
	double z;
};

int main()
{
	int x, y, z;
	std::cout << "Please enter a value of X: ...\n";
	std::cin >> x;
	std::cout << "Please enter a value of Y: ...\n";
	std::cin >> y;
	std::cout << "Please enter a value of Z: ...\n";
	std::cin >> z;
	std::cout << "Your Vector is: ";
	Vector v(x, y, z);
	v.Show();
	std::cout << "Your modul Vector is: ";
	v.ShowM();
}